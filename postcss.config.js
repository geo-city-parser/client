const { resolve } = require('path');

const rootNoExt = ({ name, type }) => resolve(process.cwd(), `src/style/${type}/`, `${name}`);

const lib = {
  'postcss-at-rules-variables': {},
  'postcss-for': {},
  'postcss-each': { afterEach: ['postcss-at-rules-variables'], beforeEach: ['postcss-css-variables'] },
  'postcss-css-variables': {},
  'postcss-calc': {},

  'postcss-url': {},
  'postcss-import': {},
  'postcss-nested': {},

  'postcss-mixins': {
    mixinsDir: rootNoExt({ name: 'mixins', type: 'pcss' }),
  },

  'postcss-responsive-type': {},
  'postcss-hexrgba': {},

  'postcss-sorting': {
    'properties-order': [
      'display',
      'margin',
      'padding',
      'border',
      'background',
      // nuxt
    ],
  },
  'postcss-pxtorem': {
    rootValue: 16,
    propList: ['*', '!border*'],
  },
  'postcss-gap-properties': {},

  'postcss-combine-duplicated-selectors': {},
};

module.exports = {
  plugins: {
    ...lib,
    cssnano: {
      preset: [
        'default',
        {
          autoprefixer: false,
          calc: false,
          discardComments: { removeAll: true },
        },
      ],
    },
  },
  order: 'presetEnvAndCssnanoLast',
  preset: {
    stage: 2,
    autoprefixer: {
      grid: true,
      flex: true,
      overrideBrowserslist: ['last 3 versions'],
    },
  },
};
