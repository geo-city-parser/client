import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import ViteComponents from 'vite-plugin-components';
import { ViteAliases } from 'vite-aliases';

// @ts-expect-error
const aliases = ViteAliases({
  prefix: '@',
});

export default defineConfig({
  plugins: [
    vue(),
    ViteComponents(),

    //
  ],
  resolve: {
    alias: aliases,
  },
});
